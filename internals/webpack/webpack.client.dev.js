const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const PATHS = require('./paths');


module.exports = require('./webpack.base.js')({
  /**
   * entry tells webpack where to start looking.
   * In this case we have both an app and vendor file.
   */
  entry: {
    index : PATHS.index,
    vendor: PATHS.vendor,
  },
  /**
   * output tells webpack where to put the files he creates
   * after running all its loaders and plugins.
   *
   * > [name].[hash].js will output something like app.3531f6aad069a0e8dc0e.js
   * > path.join(__dirname, '../build/') will output into a /build folder in
   *   the root of this prject.
   */
  output: {
    filename: 'assets/js/[name].[hash].js',
    path: PATHS.outputDir,
    publicPath: PATHS.publicPath
  },

  target: 'web',

  // devtool: 'inline-source-map',

  plugins: [

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: '[name].[hash].js',
    }),

    //Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'src/client/index.html',
      minify: {
        removeComments: false,
        collapseWhitespace: false,
        removeRedundantAttributes: false,
        useShortDoctype: false,
        removeEmptyAttributes: false,
        removeStyleLinkTypeAttributes: false,
        keepClosingSlash: false,
        minifyJS: false,
        minifyCSS: false,
        minifyURLs: false,
      },
      inject: true,
    }),

    ],
});

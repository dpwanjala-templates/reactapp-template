const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const PATHS = require('./paths');

module.exports = require('./webpack.base.js')({

  entry: {
    index: PATHS.index,
    vendor: PATHS.vendor
  },

  output: {
    filename: 'assets/js/[name].[chunkhash].js',
    chunkFilename: 'assets/js/[name].[chunkhash].chunk.js',
    path: PATHS.outputDir
  },

  target: 'web',

  devtool: 'source-map',

  plugins: [

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      warnings: false
    }),

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'src/client/index.html',
      inject: true
    })

  ]
})

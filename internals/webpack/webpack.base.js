const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const PATHS = require('./paths');


module.exports = (options) => ({
  entry: options.entry,
  output: Object.assign({
    path: PATHS.outputDir,
    publicPath: PATHS.publicPath,
  }, options.output),
  module: {
    loaders: [

    {
      test: /\.(es6|js)$/,
      exclude: /node_modules/, // ingore node_modules directory
      loader: 'babel-loader',
    },

    {
      test: /\.css$/,
      exclude: /node_modules/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    localIdentName: '[name]__[local]___[hash:base64:5]',
                    importLoaders: 1
                  }
                },

                {
                  loader: 'postcss-loader',
                  options: {
                            sourceMap: 'inline',
                        }
                }
             ]
      }),
    },

    {
      test: /\.css$/,
      exclude: /src/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
                {
                  loader: 'css-loader',
                }
             ]
      }),
    },

    {
      test: /\.(scss|sass)$/,
      exclude: /node_modules/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    localIdentName: '[name]__[local]___[hash:base64:5]',
                    importLoaders: 1
                  }
                },

                {
                  loader: 'postcss-loader',
                  options: {
                            sourceMap: 'inline',
                        }
                },

                { 
                  loader: 'sass-loader'
                }
             ]
      }),
    },

    ],
  },

  resolve: {
    modules: ['src/modules', 'src/routes', 'src/config', 'node_modules'],
    extensions: [
      '.js',
      '.jsx',
      '.css',
      '.scss',
      '.react.js',
    ]
  },

  devtool: options.devtool,

  target: options.target,

  plugins: options.plugins.concat([

      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        },
      }),

      new ExtractTextPlugin({
        filename: "assets/css/[name].css",
        allChunks: true,
        disable: false
      }),


      new webpack.NamedModulesPlugin(),

    ]),
});

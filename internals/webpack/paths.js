const path = require('path');

/*
 * __dirname is changed after webpack-ed to another directory
 * so process.cwd() is used instead to determine the correct base directory
 * Read more: https://nodejs.org/api/process.html#process_process_cwd
 */
const CURRENT_WORKING_DIR = process.cwd();

module.exports = {
  index: path.join(CURRENT_WORKING_DIR, 'src/client/index.js'),
  vendor: path.join(CURRENT_WORKING_DIR, 'src/client/vendor.js'), 
  outputDir: path.join(CURRENT_WORKING_DIR, 'dist'),
  assets: path.resolve(CURRENT_WORKING_DIR, 'public', 'assets'),
  compiled: path.resolve(CURRENT_WORKING_DIR, 'compiled'),
  contentBase: path.resolve(CURRENT_WORKING_DIR, 'public'),
  public: '/assets/', // use absolute path for css-loader?
  publicPath: '/',
  modules: path.resolve(CURRENT_WORKING_DIR, 'node_modules')
};

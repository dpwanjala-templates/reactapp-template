import React from 'react';
import Bundle from 'util/components/Bundle';
import loadContactPage from 'bundle-loader?lazy!./ContactPage';

const ContactPage = () => (
  <Bundle load={loadContactPage}>
    {(ContactPage) => <ContactPage/>}
  </Bundle>
)

export default ContactPage
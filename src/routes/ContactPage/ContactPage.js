import React from 'react';
import classNames from 'classnames/bind';
import styles from './ContactPage.scss';


const cx = classNames.bind(styles);

const ContactPage = () => {

	let className = cx({
      contactPage: true
    });

	return(<div className={className}>ContactPage</div>);
};

export default ContactPage;

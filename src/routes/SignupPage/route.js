import React from 'react';
import Bundle from 'util/components/Bundle';
import loadPage from 'bundle-loader?lazy!./SignupPage';

const Page = () => (
  <Bundle load={loadPage}>
    {(Page) => <Page/>}
  </Bundle>
)

export default Page

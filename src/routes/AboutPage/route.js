import React from 'react';
import Bundle from 'util/components/Bundle';
import loadAboutPage from 'bundle-loader?lazy!./AboutPage';

const AboutPage = () => (
  <Bundle load={loadAboutPage}>
    {(AboutPage) => <AboutPage/>}
  </Bundle>
)

export default AboutPage
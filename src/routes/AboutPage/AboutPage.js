import React from 'react';
import classNames from 'classnames/bind';
import styles from './AboutPage.scss';


const cx = classNames.bind(styles);

const AboutPage = () => {

	let className = cx({
      aboutPage: true
    });

	return(<div className={className} >AboutPage</div>);
};

export default AboutPage;

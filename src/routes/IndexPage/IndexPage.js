import React from 'react';
import classNames from 'classnames/bind';
import styles from './IndexPage.scss';

const cx = classNames.bind(styles);


const IndexPage = () => {

	let className = cx({
      indexPage: true
    });

	return(<div className={className}>IndexPage</div>);
};

export default IndexPage;

import React from 'react';
import Bundle from 'util/components/Bundle';
import loadIndexPage from 'bundle-loader?lazy!./IndexPage';

const IndexPage = () => (
  <Bundle load={loadIndexPage}>
    {(IndexPage) => <IndexPage/>}
  </Bundle>
)

export default IndexPage

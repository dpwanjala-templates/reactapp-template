import { combineEpics } from 'redux-observable';
import SignupEpics from 'signup/epics';

const { signupEpic } = SignupEpics

export const rootEpic = combineEpics(
 signupEpic
);

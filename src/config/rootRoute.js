import React from 'react';
import {Route} from 'react-router-dom';
import IndexPage from 'IndexPage/route';
import AboutPage from 'AboutPage/route';
import ContactPage from 'ContactPage/route';
import SignupPage from 'SignupPage/route';


const RootRoute = () => (
   <div>
        <Route exact path='/' component={IndexPage} />
        <Route path='/about' component={AboutPage} />
        <Route path='/contact' component={ContactPage} />
        <Route path='/signup' component={SignupPage} />
   </div>
);

export default RootRoute;

import { combineReducers } from 'redux-immutable';

function HelloWordReducer() {
  return 'Hello World !';
}

export default function createRootReducer() {
  return combineReducers({
    helloWorld: HelloWordReducer,
  });
}

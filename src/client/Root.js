import React from 'react';
import App from 'app/App';
import { Provider } from 'react-redux';
import RootRoute from '../config/rootRoute';
import {BrowserRouter as Router} from 'react-router-dom';
import configureStore from 'store';
import 'rxjs';


// Grab the state from a global variable injected into the server-generated HTML
// const preloadedState = window.__PRELOADED_STATE__;
// const initialState = preloadedState;

const initialState = {};
export const store = configureStore(initialState);

const Root = props => {
	 return (
	 	<Provider store={store}>
		 	<Router>
		 		<App>
		 			<RootRoute />
		 		</App>
		 	</Router>
	 	</Provider>
	 	)
};

export default Root;

import React from 'react';
import { render } from 'react-dom';
import Root from './Root';

const mountApp = document.getElementById('root');

render(<Root/>, mountApp);

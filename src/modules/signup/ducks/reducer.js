import { fromJS } from 'immutable';
import * as c from '../constants';

const initialState = {};

export default function SignupReducer (state = initialState, action){
	switch (action.type) {
		case c.REQUEST_SIGNUP:
			return fromJS(action.payload);
		case c.SIGNUP_FAILURE:
			return fromJS(action.payload);
		case c.SIGNUP_SUCCESS:
			return fromJS(action.payload);
	}
	return state;
}

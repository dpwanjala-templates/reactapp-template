import * as c from '../constants';

export const signupEpic = action$ =>
  action$.ofType(c.REQUEST_SIGNUP,)
    .mapTo({ type: 'SIGNING_UP' });

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignupActions from './actions';
import { withRouter } from 'react-router-dom';

class SignupView extends Component {
  render(){
    return (
      <button onClick={() => this.props.handleSignup()}>Signup</button>
    )
  }
}
function mapStateToProps(state, props){
    return {
    }
}
function matchDispatchToProps(dispatch){
    return bindActionCreators(
        {
          handleSignup: SignupActions.requestSignup
        },
        dispatch)
}
export default withRouter(connect(mapStateToProps, matchDispatchToProps)(SignupView));

import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom'


class App extends Component {

  render() {
    return (
        <div>

            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/about">About</Link></li>
              <li><Link to="/contact">Contact</Link></li>
            </ul>

          <hr/>

           {this.props.children}
           <h1>{this.props.helloWorld}</h1>

        </div>
    );
  }
}

function mapStateToProps(state, props){
    return {
      helloWorld: state.get("helloWorld")
    }
}
function matchDispatchToProps(dispatch){
    return bindActionCreators(
        {
        },
        dispatch)
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(App));
